const express = require('express')
const app = express()
const path = require('path')
const server = require('http').createServer(app)
const io = require('socket.io')(server)
const port = process.env.PORT || 8080


server.listen(port, () => {
  console.log('Server listening port: %d', port)
})

app.use(express.static(path.join(__dirname, 'public')))

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html')
})

function logMessage(message, user) {
  let date = new Date()
  let time = date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds()
  console.log(user + ' : ' + message + ' time: ' + new Date())
}

let numUsers = 0;

// connecting to socket
io.on('connection', (socket) => {
  let addedUser = false

  // Listens client emiting message and executing message
  socket.on('message', (payload) => {
    logMessage('Message: ' + payload, 'Username: ' + socket.username)

    // Broadcast the message to client to execute message
    io.emit('message', {
      username: socket.username,
      message: payload
    })

  })

  // Listens client emiting add user and executing user joined broadcast
  socket.on('add user', (username) => {
    if (addedUser) return
    logMessage('added user', username)
    // Storing username to socket session
    socket.username = username
    ++numUsers
    addedUser = true

    //emit number of current users in chat to client
    socket.emit('login', {
      numUsers: numUsers,
      nickname: socket.username
    })

    //Broadcast user who joined chat
    socket.broadcast.emit('user joined', {
      username: socket.username,
      numUsers: numUsers
    })
  })

  // When user disconnects emit username and number of current users.
  socket.on('disconnect', () => {
    if (addedUser) {
      --numUsers

      // echo globally that this client has left
      socket.broadcast.emit('user left', {
        username: socket.username,
        numUsers: numUsers
      })
    }
  })

  socket.on('typing', () => {
    socket.broadcast.emit('typing', {
      username: socket.username,
      typing: true
    })
  })

})
